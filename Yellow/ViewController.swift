//
//  ViewController.swift
//  Yellow
//
//  Created by Vailence on 15.12.2017.
//  Copyright © 2017 Vailence. All rights reserved.
//

import UIKit
import AVFoundation


class ViewController: UIViewController {


    @IBAction func sound(_ sender: Any) {
        performSegue(withIdentifier: "SoundMemorySegue", sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


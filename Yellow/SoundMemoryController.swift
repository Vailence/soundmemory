import UIKit
import AVFoundation


class SoundMemoryController: UIViewController, AVAudioPlayerDelegate {
    
    @IBOutlet weak var startGameButton: UIButton! //создание связей между объектами
    @IBOutlet var soundButton: [UIButton]! //создание связей между объектами
    @IBOutlet weak var lvlLabel: UILabel! //создание связей между объектами
    
    var sound1PLayer:AVAudioPlayer! //объявление переменной 1 звука
    var sound2PLayer:AVAudioPlayer! //объявление переменной 2 звука
    var sound3PLayer:AVAudioPlayer! //объявление переменной 3 звука
    var sound4PLayer:AVAudioPlayer! //объявление переменной 4 звука
    
    var playList = [Int]()
    var currentItem = 0
    var numberOfTaps = 0
    var readyForUser = false
    
    var level = 1  //объявление переменной уровня
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAudioFiles()
    }
   
   
    func fadeOutImg() //исчезновение кнопки
    {
        UIView.animate(withDuration: 1.5,
    delay: 0,
    options: UIViewAnimationOptions.curveLinear,
    animations: {
    self.startGameButton.alpha = 0
    }, completion: nil)
    }
    
    func setupAudioFiles(){
        
        let soundFilePath1 = Bundle.main.path(forResource: "1DO", ofType: "wav")
        let soundFileURL1 = NSURL(fileURLWithPath: soundFilePath1!)
        
        let soundFilePath2 = Bundle.main.path(forResource: "2RE", ofType: "wav")
        let soundFileURL2 = NSURL(fileURLWithPath: soundFilePath2!)
        
        let soundFilePath3 = Bundle.main.path(forResource: "3MI", ofType: "wav")
        let soundFileURL3 = NSURL(fileURLWithPath: soundFilePath3!)
        
        let soundFilePath4 = Bundle.main.path(forResource: "4FA", ofType: "wav")
        let soundFileURL4 = NSURL(fileURLWithPath: soundFilePath4!)
        
        
        do {
            try sound1PLayer = AVAudioPlayer(contentsOf: soundFileURL1 as URL)
            try sound2PLayer = AVAudioPlayer(contentsOf: soundFileURL2 as URL)
            try sound3PLayer = AVAudioPlayer(contentsOf: soundFileURL3 as URL)
            try sound4PLayer = AVAudioPlayer(contentsOf: soundFileURL4 as URL)
        }
        catch {
            print(error)
        }
        
        
        sound1PLayer.delegate = self
        sound2PLayer.delegate = self
        sound3PLayer.delegate = self
        sound4PLayer.delegate = self
        
        
        sound1PLayer.numberOfLoops = 0
        sound2PLayer.numberOfLoops = 0
        sound3PLayer.numberOfLoops = 0
        sound4PLayer.numberOfLoops = 0
        
    }
    
    
    @IBAction func soundButtonPressed(_ sender: Any) {
        
        if readyForUser {
            let button = sender as! UIButton
            
            switch button.tag {
            case 1:
                sound1PLayer.play()
                checkIfCorrect(buttonPressed: 1)
                break
            case 2:
                sound2PLayer.play()
                checkIfCorrect(buttonPressed: 2)
                break
            case 3:
                sound3PLayer.play()
                checkIfCorrect(buttonPressed: 3)
                break
            case 4:
                sound4PLayer.play()
                checkIfCorrect(buttonPressed: 4)
                break
            default:
                break
            }
        }
    }
    
    
    
    func checkIfCorrect (buttonPressed:Int)
    {
        if buttonPressed == playList[numberOfTaps]
        {
            print("buttonPressed")
            if numberOfTaps == playList.count - 1
            {
                print ("numberofTaps")
                
                let deadlineTime = DispatchTime.now() + .seconds(1)
                DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
                    self.nextRound()
                }
                return
            }
            numberOfTaps += 1
        }
        else
        {
            resetGame()
        }
    }
    
    
    func resetGame()
    {
        level = 1
        readyForUser = false
        numberOfTaps = 0
        currentItem = 0
        playList = []
        lvlLabel.text = "GAME OVER"
        startGameButton.isHidden = false
        disableButtons()
    }
    
    
    

    
    
    func nextRound ()
    {
        level += 1
        lvlLabel.text = "Level \(level)"
        readyForUser = false
        numberOfTaps = 0
        currentItem = 0
        disableButtons()
        
        let randomNumber = Int(arc4random_uniform(4) + 1)
        playList.append(randomNumber)
        playNextItem()
        
    }
    
    
    
    @IBAction func startGame(_ sender: Any) {
        lvlLabel.text = "Level 1"
        fadeOutImg()
        //        disableButtons()
        let randomNumber = Int(arc4random_uniform(4) + 1)
        playList.append(randomNumber)
//        startGameButton.isHidden = true
        playNextItem()
    }
    
    
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        if currentItem <= playList.count - 1 {
            playNextItem()
        }
        else {
            readyForUser = true
            resetButtonHiglights()
            enableButtons()
        }
    }
    
    func playNextItem() {
        let selectedItem = playList[currentItem]
        
        switch selectedItem {
        case 1:
            higlightButtonWithTag(tag: 1)
            sound1PLayer.play()
            break
        case 2:
            higlightButtonWithTag(tag: 2)
            sound2PLayer.play()
            break
        case 3:
            higlightButtonWithTag(tag: 3)
            sound3PLayer.play()
            break
        case 4:
            higlightButtonWithTag(tag: 4)
            sound4PLayer.play()
            break
        default:
            break;
        }
        currentItem += 1
    }
    
    
    
    func higlightButtonWithTag (tag:Int) // функция подсвечивания кнопки при нажатии
    {
        switch tag {
        case 1:
            resetButtonHiglights()
            soundButton[tag - 1].setImage(UIImage(named:"redPressed"), for: [.normal])
        case 2:
            resetButtonHiglights()
            soundButton[tag - 1].setImage(UIImage(named:"yellowPressed"), for: [.normal])
        case 3:
            resetButtonHiglights()
            soundButton[tag - 1].setImage(UIImage(named:"bluePressed"), for: [.normal])
        case 4:
            resetButtonHiglights()
            soundButton[tag - 1].setImage(UIImage(named:"greenPressed"), for: [.normal])
        default:
            break
        }
    }
    
    
    func resetButtonHiglights () //сброс подсветки
    {
        soundButton[0].setImage(UIImage(named: "red"), for: [.normal])
        soundButton[1].setImage(UIImage(named: "yellow"), for: [.normal])
        soundButton[2].setImage(UIImage(named: "blue"), for: [.normal])
        soundButton[3].setImage(UIImage(named: "green"), for: [.normal])
        
    }
    
    
    func disableButtons ()
    {
        for button in soundButton
        {
            button.isUserInteractionEnabled = false
        }
    }
    
    func enableButtons ()
    {
        for button in soundButton
        {
            button.isUserInteractionEnabled = true
        }
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}


